GroupDocs.Annotation is a web tool for annotating documents within your browser.
It allows you work on documents without any additional software, libraries or plugins, just open the browser and you are ready to go.
It is a library with a wide file format support (more than 45 formats).
These include PDF, Microsoft Office documents, images, emails, OpenDocument formats, HTML and others.

-- INSTALLATION --

1. Unpack the groupdocs_annotation_dotnet folder and contents in the appropriate modules
   directory of your Drupal installation. This is probably sites/modules/
   Make sure that groupdocs_annotation_dotnet folder is writable.
2. Enable Embedded Groupdocs Annotation for .NET in the admin modules section.
3. Click on "manage fields" under Home » Administration » Structure and
   add new field with type "GroupDocs Annotation  for .NET".
4. Go edit or add new node of type that you added new field to. Click "Choose file" to open pop-up, in which you need enter server of url, where was installed your app GroupDocs.Annotation for .NET .


   Note: Current version of module support creation of only one field!
