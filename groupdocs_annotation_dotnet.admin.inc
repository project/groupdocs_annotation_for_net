<?php
/**
 * @file
 * Functions relating to the admin of this module
 */

/**
 * Form constructor for the file uploadingform.
 *
 * @see groupdocs_signature_menu()
 */
function groupdocs_annotation_dotnet_uploading_form($form, &$form_state)
{
    $form['url'] = array(
        '#type' => 'textfield',
        '#title' => t('Server url'),
        '#default_value' => '',
        '#size' => 64,
        '#maxlength' => 255,
        '#description' => t("You must enter server url where installed Dotnet annotation."),
    );

    $form['width'] = array(
        '#type' => 'textfield',
        '#title' => t('Width'),
        '#default_value' => '',
        '#size' => 10,
        '#maxlength' => 255,

    );

    $form['height'] = array(
        '#type' => 'textfield',
        '#title' => t('Height'),
        '#default_value' => '',
        '#size' => 10,
        '#maxlength' => 255,

    );


    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Go!'),
    );

    return $form;
}

/**
 * Form submission handler for groupdocs_annotation_dotnet_uploading_form().
 *
 * groupdocs_annotation_dotnet_uploading_form submit process
 * move upload file to module directory
 * call Groupdocs Api to upload file
 * using private and public keys from config page
 * unset file fom module directory.
 *
 * Call javascript function that will pass file guid to
 * parent window's test input
 *
 * @see groupdocs_signature_uploading_form()
 */
function groupdocs_annotation_dotnet_uploading_form_submit($node, &$form_state)
{

    $url = strip_tags(trim($_POST['url']));
    $width = strip_tags(trim($_POST['width']));
    $height = strip_tags(trim($_POST['height']));
    if ($url == '' or $height == '' or $width == ''){
        print t("Please, fill all parameters."); exit;
    }else{


    print "<script>
    window.parent.jQuery('input[name*=\"groupdocs_annotation_dotnet_id\"]').val('" . $url . ";width:" . $width . "px;height:" . $height  ."px');
    window.parent.Lightbox.end()
    </script>";
    drupal_exit();
    }
}
